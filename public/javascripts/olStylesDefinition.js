//==========================================Colour styles initialization============================================
/**
 * Стиль текста на карте
 * @type {ol.style.Style}
 */
let text = new ol.style.Style({
    text: new ol.style.Text({
        text: '',
        overflow: true,
        fill: new ol.style.Fill({
            color: 'grey'
        }),
        stroke: new ol.style.Stroke({
            color: 'black'
        })
    })
});

let textHighlight = new ol.style.Style({
    text: new ol.style.Text({
        text: '',
        overflow: true,
        fill: new ol.style.Fill({
            color: 'white'
        }),
        stroke: new ol.style.Stroke({
            color: 'black'
        }),
        font: 'bold 20px "Sans-Serif", "Montserrat"'
    })
});

//==========================================Style functions initialization==========================================
/**
 * Стайлинговая функция, которая возвращает стиль в зависимости от того какая группа слоев сейчас отображается
 * @param feature - входной объект, к которому применится стиль
 * @returns {*} - стиль
 */
function buildingStyleFunction(feature) {
    //  если объект выделен
    if (feature.get(layerBuildingSelection.attribute) === selectedFeatureID) {
        return layerBuildingSelection.style;
    } else {
        let group = currentLayerGroup; // иначе определяем текущую группу слоев
        for (i = 0; i < group.length; i++) { // и применяем стайлинговую функцию
            let layer = group[i];
            if (layer.visible) {
                if (feature.get(layer.attribute) > layer.minVal &&
                    feature.get(layer.attribute) < layer.maxVal) {
                    return layer.style;
                }
            }
        }
    }
}

/**
 * Стили регионов
 * @param feature
 * @param resolution
 * @returns {*}
 */
function regionStyleFunction(feature, resolution) {
    let layer = layerRegionSelection; // первый стиль селекшена
    if (feature.get(layer.attribute) === selectedFeatureID) {
        return layer.style;
    } else {
        let gsLayerName = feature.getProperties()['layer'];
        // let resolution = map.getView().getResolution();
        if (gsLayerName === 'boundary_level_4_shp') {
            return admin_lvl_4_style
        } else if (gsLayerName === 'boundary_level_6_shp' && resolution < adminLvl6MaxResolution) {
            return admin_lvl_6_style
        } else if (gsLayerName === 'boundary_level_8_shp' && resolution < adminLvl8MaxResolution) {
            return admin_lvl_8_style
        }
    }
}

/**
 * Стили подписей регионов
 * @param feature
 * @param resolution
 * @returns {ol.style.Style}
 */
let labelStyle = function (feature, resolution) {
    let adminLvl = feature.get('ADMIN_LVL');
    let regionName = feature.get('NAME');
    text.getText().setText(regionName);

    if (feature.get(layerRegionSelection.attribute) === selectedFeatureID) {
        textHighlight.getText().setText(regionName);
        return textHighlight;
    }

    if (adminLvl === '4' && resolution > adminLvl6MaxResolution) {
        text.getText().setFont('bold 17 px "Sans-Serif", "Montserrat"');
        return text;
    } else if (adminLvl === '6'
        && resolution <= adminLvl6MaxResolution
        && (resolution > adminLvl8MaxResolution
            || regionName === 'городской округ Череповец'
            || regionName === 'городской округ Вологда')) {
        text.getText().setFont('bold 12px "Sans-Serif", "Montserrat"');
        return text;
    } else if (adminLvl === '8' && resolution <= adminLvl8MaxResolution) {
        text.getText().setFont('bold 12px "Sans-Serif", "Montserrat"');
        return text;
    }
};


//================================================Region layers========================================================
var admin_lvl_4_style = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: '#2fa24e',
        width: 2
    }),
    zIndex: 4
});

var admin_lvl_6_style = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: '#999999',
        width: 1
    }),
    zIndex: 3
});

var admin_lvl_8_style = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: '#06c6c9',
        width: 1
    }),
    zIndex: 2
});


//================================================Houses layers========================================================
// Слой для отображения неактивированных слоев
var defaultStyle = new ol.style.Style({});

var selectStyle = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(0, 0, 255, 0.1)'
    }),
    stroke: new ol.style.Stroke({
        color: '#506bff'
    }),
    zIndex: 5
});


var violet = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#3E1B3C'
    })
});

var red_deep = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#820333'
    })
});

var red = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#C9283E'
        
    })
});

var orange = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#FF6138'
    })
});

var yellow_warm = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#FFDD5C'
    })
});

var yellow_cold = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#FFFF9D'
    })
});

var blue_light = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#A2F2EA'
    })
});

var green_cold = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#69F4BD'
    })
});

var green_light = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#96ED89'
    })
});

var green = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#45BF55'
    })
});

var green_dark = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#167F39'
    })
});

var green_deep = new ol.style.Style({
    fill: new ol.style.Fill({
        color: '#044C29'
    })
});