// API key for mapbox server. Necessary until we don't have customized geoserver
var apiKey = 'pk.eyJ1IjoibGlkaXlhZXYiLCJhIjoiY2phMzdzcmk2NnZ2czMybXZianJjM2M1ayJ9.bgMlxj3jSfIbJg8u_hwO8g';

// Geoserver host
var geoserverHost = "kapremontdom.ru";
// var geoserverHost = "localhost:8080";
var format = 'image/png';

// Имена слоев
const housesLayerName = 'houses';
const adminRegionsLayerName = 'admin_regions';
const adminRegionsLabelsLayerName = 'region_labels';

// Говнокод (глобальные переменные)
let map;
let overlay;

// Отображаемый фильтр домов (по году постройки или износу)
var currentLayerGroup = layersBuildYear;

// Координаты и ограничивающие прямоугольники разных субъектов
var selectedFeatureID = null;

var vologdaBB = [3832854, 8045011, 5261310, 8783698];
var vologdaCentr = [4440646, 8227590];

// Масштабы различных субъектов
const cityMaxResolution = 20;
const regionMaxResolution = 2000;
const adminLvl8MaxResolution = 300;
const adminLvl6MaxResolution = 1700;
const adminLvl4MaxResolution = 2500;

// TODO: переписать на более адекватную реализацию, чтобы зависила от кнопок напрямую
// // Инициализация диапазонов для аттрибута build_year, все подслои включены
let numberOfYearRanges = 11;
let yearToggles = new Array(numberOfYearRanges);
for (i = 0; i < numberOfYearRanges; i++) {
    yearToggles[i] = true;
}
// Инициализация диапазонов для аттрибута wearout, все слои отключены
let numberOfWearoutRanges = 5;
let wearoutToggles = new Array(numberOfWearoutRanges);
for (i = 0; i < numberOfWearoutRanges; i++) {
    wearoutToggles[i] = true;
}

//====================================================Map functions=====================================================
/**
 * Функция запускающая карту после генерации страницы
 */
function init() {
    document.removeEventListener('DOMContentLoaded', init);

    let viewport = document.getElementById('map');

    function getMinZoom() {
        let width = viewport.clientWidth;
        return Math.ceil(Math.LOG2E * Math.log(width / 256) + 4);
    }

    let initialZoom = getMinZoom();


    //=================================================Popup with building details======================================
    // Elements that make up the popup.
    let container = document.getElementById('popup');
    let content = document.getElementById('popup-content');
    let closer = document.getElementById('popup-closer');
    // Create an overlay to anchor the popup to the map.
    overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
        element: container
        // autoPan: true,
        // autoPanMargin: 250,
        // autoPanAnimation: {
        //     duration: 250
        // }
    }));
    overlay.setPosition(undefined);

    // Add a click handler to hide the popup.
    closer.onclick = function () {
        overlay.setPosition(undefined);
        deselectAllObjects();
        closer.blur();
        return false;
    };

    //================================================Layers initialization=============================================
    // Административное деление
    let adminRegions = new ol.layer.VectorTile({
        name: adminRegionsLayerName,
        style: regionStyleFunction,
        source: new ol.source.VectorTile({
            format: new ol.format.MVT(),
            url: 'http://' + geoserverHost + '/geoserver/gwc/service/tms/1.0.0/' + gsWorkspace + ':'
            + gsAdminRegionsLayerName + '@EPSG:900913' + '@pbf/{z}/{x}/{-y}.pbf',
        }),
        renderMode: 'vector',
        updateWhileInteracting: true,
        updateWhileAnimating: true,
        preload: 3,
        extent: vologdaBB,
    });

    let adminRegionsLabels = new ol.layer.Vector({
        name: adminRegionsLabelsLayerName,
        style: labelStyle,
        source: new ol.source.Vector({
            format: new ol.format.GeoJSON(),
            url: 'http://' + geoserverHost + '/geoserver/wfs?service=WFS&' +
            'version=1.1.0&request=GetFeature&typename='
            + gsWorkspace + ':' + adminRegionsLabelsLayerName + '&' +
            'outputFormat=application/json&srsname=EPSG:3857&'
        }),
        renderMode: 'vector',
        overlaps: false,
        updateWhileInteracting: true,
        updateWhileAnimating: true,
    });

    var houses = new ol.layer.VectorTile({
        name: housesLayerName,
        style: buildingStyleFunction,
        source: new ol.source.VectorTile({
            format: new ol.format.MVT(),
            url: 'http://' + geoserverHost + '/geoserver/gwc/service/tms/1.0.0/' + gsWorkspace + ':'
            + gsHousesLayerName + '@EPSG%3A' + '900913' + '@pbf/{z}/{x}/{-y}.pbf'
        }),
        maxResolution: cityMaxResolution,
        renderMode: 'image',
        updateWhileInteracting: true, // повышает плавность анимации за счет большей нагрузки на цп
        updateWhileAnimating: true, // повышает плавность анимации за счет большей нагрузки на цп
        preload: 3,
        zIndex: 10,
        extent: vologdaBB
    });

    // let adminRegions2 = new ol.layer.Tile({
    //     source: new ol.source.TileWMS({
    //         url: "http://" + geoserverHost + "/geoserver/wms",
    //         params: {'LAYERS': 'vologodskaya_oblast:region_boundaries', 'TILED': true},
    //         serverType: 'geoserver',
    //         crossOrigin: 'anonymous'
    //     })
    // });

    let background = new ol.layer.Tile({ // Черная подложка
        source: new ol.source.XYZ({
            url: 'https://api.mapbox.com/v4/mapbox.dark/{z}/{x}/{y}.png?access_token=' + apiKey
        }),
        updateWhileInteracting: true,
        updateWhileAnimating: true,
        preload: 3,
        zIndex: 0
    });

    //================================================View initialization===============================================
    let view = new ol.View({
        minZoom: 6,
        maxZoom: 17,
        center: vologdaCentr,
        zoom: 14,
        extent: vologdaBB,
        // resolutions: adminRegions.getSource().getTileGrid().getResolutions()
    });

    //============================================Interactions initialization===========================================


    //============================================Map initialization====================================================
    map = new ol.Map({
        target: 'map',
        layers: [
            background,
            // TODO: Удалить после окончания тестирования
            //     new ol.layer.Tile({
            //         source: new ol.source.OSM()
            //     }),
            adminRegions,
            adminRegionsLabels,
            houses
        ],
        // interactions: ol.interaction.defaults().extend([select]),
        overlays: [overlay],
        view: view,
        controls: [
            //Define the default controls
            new ol.control.Zoom(),
            //Define some new controls
            new ol.control.ZoomSlider(),
            new ol.control.MousePosition(),
            new ol.control.ScaleLine(),
        ]
    });

    // map.on('singleclick', selectFeature);
    // TODO: удалить функцию тестирования
    map.on('singleclick', selectObject);
}


//============================================Additional Functions==================================================
// TODO: доделать чтобы мог работать с площадью полигонов
/**
 * Выбирает объект по нажатию. Выбор включает в себя подсветку, запрос информации, отображение модального окна
 * @param event - событе триггернувшее функцию
 */
function selectObject(event) {
    let features = map.getFeaturesAtPixel(event.pixel);

    // TODO: придумать что с этим сделать
    // Необхожимо отсортировать слои так чтобы первым был слой с домами, а дальше слои регионов начиная с самого детального уровня и вверх по масштабу
    features.sort(function (a, b) {
        let aGsLayerName = a.getProperties()['layer'];
        let bGsLayerName = b.getProperties()['layer'];
        if (aGsLayerName === housesLayerName) return -1;
        else if (aGsLayerName > bGsLayerName) return -1;
        else return 1;
    });

    if (!features) {
        console.log("There is no features at pixel");
        return;
    }
    let hasPrevious = false; // необходима для определния иерархии слоев

    // поиск подходящего слоя
    for (let feature of features) {
        let gsLayerName = feature.getProperties()['layer'];
        console.log("Resolution = " + map.getView().getResolution() + ", layer: " + gsLayerName);
        //  Функция для выбора слоя в соответствии с масштабом
        if (isZoomSuitableLayer(map.getView().getResolution(), gsLayerName, hasPrevious)) {
            hasPrevious = true;
            console.log("Layer name = " + gsLayerName);
            // TODO: добавить стандартный uid к любому типу отображаемых фич.
            let attributeName = gsLayerName === gsHousesLayerName ? 'osm_id' : 'OSM_ID';
            let attributeValue = gsLayerName === gsHousesLayerName ? feature.getProperties().OSM_ID : feature.getProperties().OSM_ID;

            // Если выбранный объект совпадает с текущим объектом, то октлючить выделение
            if (selectedFeatureID !== null) {
                deselectAllObjects();
                return;
            }

            highlightObject(attributeValue);
            // TODO: костыль, до момента пока мы не сможем искать по регионам с помощью osm_id
            if (gsLayerName === gsHousesLayerName) {
                // Добавеление модального окна
                moveToHouse(event.coordinate);
                overlay.setPosition(event.coordinate);
                createPopup(overlay, selectedFeatureID);
            } else {
                // document.getElementById("popup").style.visibility = 'hidden';
                overlay.setPosition(undefined);
            }
            break;
        }
    }

    /**
     * Приватная фукнция. Определяет какой слой должен быть активирован по клику в зависимости от масштаба и
     * наличия других слоев
     * @param resolution - текущий масштаб
     * @param geoserverlInternalLayerName - название слоя, которое используется в геосервере
     * @param hasPrevious - имеется ли более приоритетный слой по иерархии слоев (дома > район > область..)
     * @returns {boolean} - нужно ли активировать текущий слой
     */
    function isZoomSuitableLayer(resolution, geoserverlInternalLayerName, hasPrevious) {
        if (geoserverlInternalLayerName === gsHousesLayerName) {
            return resolution < cityMaxResolution;
        } else if (geoserverlInternalLayerName === gsAdminLVL8LayerName) {
            if (resolution < adminLvl8MaxResolution) {
                return resolution >= cityMaxResolution || !hasPrevious
            }
        } else if (geoserverlInternalLayerName === gsAdminLVL6LayerName) {
            if (resolution < adminLvl6MaxResolution) {
                return resolution >= adminLvl4MaxResolution || !hasPrevious
            }
        } else if (geoserverlInternalLayerName === gsAdminLVL4LayerName) {
            if (resolution < adminLvl4MaxResolution) {
                return resolution >= adminLvl6MaxResolution || !hasPrevious
            }
        }
        return false;
    }
}

/**
 * Снимает все текущие выделения и закрывает модальные окна
 */
function deselectAllObjects() {
    selectedFeatureID = null;
    reloadStyle(map);
    overlay.setPosition(undefined);
    console.log("Deselect object");
}

/**
 * Подсвечивает объект
 * @param featureID
 */
function highlightObject(featureID) {
    selectedFeatureID = featureID;
    console.log("selected featureId = " + selectedFeatureID);
    // Подсветка выделенного объекта
    reloadStyle(map);
}

function moveToHouse(coord) {
    let size = map.getSize();
    map.getView().animate({
        center: [coord[0] + size[0] / 4, coord[1] + size[1] / 3],
        duration: 1000,
        zoom: 22
    });
}

// TODO: Лида, этот метод нужно использовать для приближения к дому, который вы найдете через поиск
/**
 * Функция для зуммирования в регион по уникальному идентификатору.
 * @param featureID - уникальный идентификатор
 * @param duration - время анимации в милисекундах
 */
function zoomToRegion(featureID, duration) {
    console.log("In zoom to region");
    // Делаем запрос на получение объектов
    featureWFSRequest(featureID, function (features) {
        // Если объектов > 1
        if (features.length > 1) {
            console.log('zoomToRegion: More than one feature selected')
        }
        // Всегда должен быть только один объект (уникальный OSM_ID), поэтому берем features[0]
        let feature = features[0];
        console.log(feature);

        let extent = (feature.getGeometry().getExtent()); //ограничиающий прямоугольник
        let coord = ol.extent.getCenter(extent);

        highlightObject(featureID);
        overlay.setPosition(coord);
        // createPopup(overlay, featureID);
        // TODO: ограничить взаимодействия с картой на время анимации
        map.getView().fit(extent, {
            size: [500, 500],
            padding: [400, 300, 0, 0], // TODO: надо настроить точное положение на экране
            constrainResolution: true,
            maxZoom: 24,
            duration: duration
        });
    });
}

function createPopup(rootElement, OSM_ID) {
    console.log("In create popup function, OSM_ID=" + OSM_ID);
    $("#popup-content").ready(function () {
        document.getElementById("popup").style.visibility = 'visible';
        console.log("ready popup");
        loadModal(OSM_ID);
    });

    // $("#popup-content").load('home-info')
    $('[data-toggle="tab"]').click(function (e) {
        console.log("click");
        e.preventDefault();
        $(this).tab('show')
    });
}


//============================================-Debug Functions======================================================
function showInfo(event) {
    var features = map.getFeaturesAtPixel(event.pixel);
    if (!features) {
        return;
    }
    var properties = features[0].getProperties();
    console.log(properties);
}


//============================================Geoserver Request Functions===============================================
/**
 * Запрашивает полную информацию об отдельном объекте. Включает метаинформацию и геометрию объекта.
 * Среднее время обращения 800мс
 * @param featureID - уникальный идентификатор объекта по которому мы ищем его в БД
 * @param callback - функция, выполняющая дальнейшие действия с вернувшимися объектами
 */
function featureWFSRequest(featureID, callback) {
    // Фильтр
    CQLquery = 'OSM_ID=' + featureID;
    // создаем GetFeature request
    let featureRequest = new ol.format.WFS().writeGetFeature({
        // srsName: 'EPSG:900913', // проекция
        featureNS: gsWorkspaceNS, // Name URI в настройках воркспейса
        featurePrefix: gsWorkspace, // название воркспейса
        featureTypes: [gsHousesLayerName], // название слоя
        outputFormat: 'application/json',
        filter: ol.format.filter.equalTo('OSM_ID', featureID)
    });
    // Куда делать POST запрос

    let url = 'http://' + geoserverHost + '/geoserver/' + gsWorkspace + '/wfs';
    fetch(url, {
        method: 'POST',
        body: new XMLSerializer().serializeToString(featureRequest)
    }).then(function (response) {
        return response.json();
    }).then(function (json) {
        callback(new ol.format.GeoJSON().readFeatures(json, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        }));
    });
}

//===============================================Functions for LAYERS===================================================

/**
 * Перезагружает группу слоев при смене атрибута для фильтрации (по году/износу)
 * @param layerGroup - группа слоев на которую переключились
 */
function switchLayerGroup(layerGroup) {
    console.log('switched layer gorup');
    deselectAllObjects();

    if (layerGroup === currentLayerGroup) {
        console.log("switchLayerGroup: must not invoke this function")
    } else {
        // делает видимыми слои текущей группы
        for (let i = 0; i < layerGroup.length; i++) {
            layerGroup[i].visible = true;
        }
        // делает невидимыми слои предыдущей группы
        for (let i = 0; i < currentLayerGroup.length; i++) {
            currentLayerGroup[i].visible = false;
        }
        currentLayerGroup = layerGroup;
        reloadStyle(map);
    }
}

/**
 * Переключает слой из режима видимости в невидимость и наоборот.
 * @param subLayer - фильтруемый диапазон назначенный на кнопку
 * @param layerGroup - группа слоев по атрибуту (по году, износу)
 * @param map
 */
function toggleSubLayer(subLayer, layerGroup, button) {
    // console.log("Toggled " + button.id);
    subLayer.visible = !subLayer.visible;
    button.style.background = subLayer.visible ? subLayer.style.getFill().getColor() : '#b5b5b5';
    reloadStyle(map);
}

/**
 * Применяет стайлинговую функцию в зависимости от того какие подслои включены для определенной группы слоев
 * @param map - карта на которой находятся слои
 */
function reloadStyle(map) {
    map.getLayers().forEach(function (layer) {
        layer.changed();
    })
}

document.addEventListener('DOMContentLoaded', init);