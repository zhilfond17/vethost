// TODO: Это временное решение, все будет переделано на нормальную структуру, типа мапы со слоями
//================================================Слой фильтруемый по OSM_ID============================================

const attributeOSM_ID = 'OSM_ID';

let layerBuildingSelection = {
    name: "layerBuildingSelection",
    attribute: attributeOSM_ID,
    style: selectStyle,
};

// TODO: после унификации id нужно будет поменять аттрибут на attributeOSM_ID
let layerRegionSelection = {
    name: "layerRegionSelection",
    attribute: 'OSM_ID',
    style: selectStyle
};

//============================================Слой фильтруемый по build_year============================================
// Название аттрибута в shp файле
let attributeBuildYearNameInSource = 'build_year';

// Описание подслоев, с указанием имени, имени атрибута, стиля из таблицы стилей, и диапазона значений фильтрации
let layerBuild1909 = {
    name: "layerBuild1909",
    attribute: attributeBuildYearNameInSource,
    style: violet,
    minVal: 0,
    maxVal: 1910,
    visible: true
};
let layerBuild1910 = {
    name: "layerBuild1910",
    attribute: attributeBuildYearNameInSource,
    style: red_deep,
    minVal: 1909,
    maxVal: 1920,
    visible: true
};
let layerBuild1920 = {
    name: "layerBuild1920",
    attribute: attributeBuildYearNameInSource,
    style: red,
    minVal: 1919,
    maxVal: 1930,
    visible: true
};
let layerBuild1930 = {
    name: "layerBuild1930",
    attribute: attributeBuildYearNameInSource,
    style: orange,
    minVal: 1929,
    maxVal: 1940,
    visible: true
};
let layerBuild1940 = {
    name: "layerBuild1940",
    attribute: attributeBuildYearNameInSource,
    style: yellow_warm,
    minVal: 1939,
    maxVal: 1950,
    visible: true
};
let layerBuild1950 = {
    name: "layerBuild1950",
    attribute: attributeBuildYearNameInSource,
    style: yellow_cold,
    minVal: 1949,
    maxVal: 1960,
    visible: true
};
let layerBuild1960 = {
    name: "layerBuild1960",
    attribute: attributeBuildYearNameInSource,
    style: blue_light,
    minVal: 1959,
    maxVal: 1970,
    visible: true
};
let layerBuild1970 = {
    name: "layerBuild1970",
    attribute: attributeBuildYearNameInSource,
    style: green_cold,
    minVal: 1969,
    maxVal: 1980,
    visible: true
};
let layerBuild1980 = {
    name: "layerBuild1980",
    attribute: attributeBuildYearNameInSource,
    style: green_light,
    minVal: 1979,
    maxVal: 1990,
    visible: true
};
let layerBuild1990 = {
    name: "layerBuild1990",
    attribute: attributeBuildYearNameInSource,
    style: green,
    minVal: 1989,
    maxVal: 2000,
    visible: true
};
let layerBuild2000 = {
    name: "layerBuild2000",
    attribute: attributeBuildYearNameInSource,
    style: green_dark,
    minVal: 1999,
    maxVal: 2010,
    visible: true
};
let layerBuild2010 = {
    name: "layerBuild2010",
    attribute: attributeBuildYearNameInSource,
    style: green_deep,
    minVal: 2009,
    maxVal: 2020,
    visible: true
};

let layersBuildYear = [layerBuild1909, layerBuild1910, layerBuild1920, layerBuild1930, layerBuild1940, layerBuild1950,
    layerBuild1960, layerBuild1970, layerBuild1980, layerBuild1990, layerBuild2000, layerBuild2010];

//============================================Слой фильтруемый по wearout===============================================
// TODO: дописать
let attributeWearoutNameInSource = 'wearout';

let layerWearout0 = {
    name: "layerWearout0",
    attribute: attributeWearoutNameInSource,
    style: green,
    minVal: -1,
    maxVal: 20,
    visible: false
};
let layerWearout20 = {
    name: "layerWearout20",
    attribute: attributeWearoutNameInSource,
    style: green_cold,
    minVal: 19,
    maxVal: 40,
    visible: false
};
let layerWearout40 = {
    name: "layerWearout40",
    attribute: attributeWearoutNameInSource,
    style: yellow_warm,
    minVal: 39,
    maxVal: 60,
    visible: false
};
let layerWearout60 = {
    name: "layerWearout60",
    attribute: attributeWearoutNameInSource,
    style: red,
    minVal: 59,
    maxVal: 80,
    visible: false
};
let layerWearout80 = {
    name: "layerWearout80",
    attribute: attributeWearoutNameInSource,
    style: red_deep,
    minVal: 80,
    maxVal: 101,
    visible: false
};

let layersWearout = [layerWearout0, layerWearout20, layerWearout40, layerWearout60, layerWearout80];