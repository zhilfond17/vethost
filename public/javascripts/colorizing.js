function applyStyle(layer, attribute) {
    console.log("Applying style");

    // if (layer.get('headers')[attribute] === 'string') {
    //     this.messages.textContent = 'A numeric column is required for graduated symbology.';
    // } else {
        var attributeArray = [];
        layer.getSource().forEachFeature(function (feat) {
            attributeArray.push(feat.get(attribute));
        });
    // }
    var max = Math.max.apply(null, attributeArray);
    var min = Math.min.apply(null, attributeArray);
    var step = (max - min) / 5;

    var colors = [[254, 240, 17], [200, 150, 187], [0, 0, 0], [130, 80, 255], [32, 54, 60]];

    layer.setStyle(function (feature, res) {
        var property = feature.get(attribute);
        var color = property < min + step * 1 ? colors[0] :
            property < min + step * 2 ? colors[1] :
                property < min + step * 3 ? colors[2] :
                    property < min + step * 4 ? colors[3] : colors[4];
        var style = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: [0, 0, 0, 1],
                width: 1
            }),
            fill: new ol.style.Fill({
                color: color
            })
        });
        return [style];
    });
}