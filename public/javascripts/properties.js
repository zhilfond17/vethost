const gsHousesLayerName = 'houses';
const gsAdminLVL4LayerName = 'boundary_level_4_shp';
const gsAdminLVL6LayerName = 'boundary_level_6_shp';
const gsAdminLVL8LayerName = 'boundary_level_8_shp';
const gsAdminRegionsLayerName = 'region_boundaries';
const gsWorkspace = 'vologodskaya_oblast';
const gsWorkspaceNS = 'http://localhost/vologodskaya_oblast/';