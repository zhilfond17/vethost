var express = require('express');
var router = express.Router();
let requests = require('../models/request')
let ejs = require('ejs')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/application',(req,res,next)=>{
    let group = req.body.topic
    let title = req.body.title
    let desc = req.body.description
    let userId = req.body.userid
    let houseId = req.body.houseid
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    requests.addRequest(userId,houseId,group,title,desc,date,(error,result)=>{
      if (error) console.error("new application error: ",error)
      else{
        console.log("application created: ",result)
        res.send(result)
      }
    })


})

router.get('/application/user/:userid',(req,res,next)=>{
    let id = req.params.userid
    requests.getRequestByUser(id,(error,result)=>{
        if (error) console.error("fetch application error: ",error)
        else{
            console.log("application fetched: ",result)
            ejs.renderFile(__dirname + '/../views/user_profile/applications_table_history.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})

router.get('/application/house/:houseid',(req,res,next)=>{
    let id = req.params.houseid
    requests.getRequestsByHouse(id,(error,result)=>{
        if (error) console.error("fetch application error: ",error)
        else{
            console.log("application fetched: ",result)
            ejs.renderFile(__dirname + '/../views/user_profile/applications_table_history.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})


router.get('/application/app/:id',(req,res,next)=>{
    let id = req.params.id
    requests.getRequest(id,(error,result)=>{
        if (error) console.error("application fetch error: ", error)
        else {
            console.log("application fetched: ", result)
            ejs.renderFile(__dirname + '/../views/user_profile/user_app.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})



router.get('/photos',(req,res,next)=>{
  let id =req.body.id
})
module.exports = router;
