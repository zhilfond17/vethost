let router = require('express').Router()
let requests = require('../models/request')
let comments =require('../models/comment')
let users = require('../models/user')
let ejs = require('ejs')


router.post('/changestatus',(req,res,next)=>{
    let id = req.body.id
    let status = req.body.status
    let statusEnumValue
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    switch (status){
        case '1': statusEnumValue = 'Не относится к программе'
            break
        case '2': statusEnumValue = 'На рассмотрении'
            break
        case '3': statusEnumValue = 'В процессе ремонта'
            break
        case '4': statusEnumValue = 'Завершено'
            break
        case '5': statusEnumValue = 'Не рассмотрено'
            break
    }
    requests.updateRequestStatus(id,statusEnumValue,date,(error,result)=>{
        if (error)console.error("status update error: ", error)
        else {
            console.log("status updated: ", result)
            res.send(result)
        }
    })

})

router.get('/application/:id',(req,res,next)=>{
    let id = req.params.id
    requests.getRequest(id,(error,result)=>{
        if (error) console.error("application fetch error: ", error)
        else {
            if (result.length>0){
                for (let i=0;i<result.length;i++){
                    let currentUserId = result[i].id_user
                    users.findUserById(currentUserId,(error,usersResult)=>{
                        if (error || usersResult.length<1) {
                            console.error("user shit happened: ",error)
                            res.render('error',{})

                        }
                        else {
                            result[i].username =usersResult[0].name
                        }
                    })
                }
            }
            console.log("application fetched: ", result)
            ejs.renderFile(__dirname + '/../views/gos_org_profile/process_application.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })

})

router.get('/application/all',(req,res,next)=>{
    requests.getAllRequests((error,result)=>{
        if (error) console.error("applications fetch error: ", error)
        else {
            console.log("applications fetched: ", result)
            ejs.renderFile(__dirname + '/../views/gos_org_profile/inbox_table.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})

router.get('/status/process/:status',(req,res,next)=>{
    let status = req.params.status
    let statusEnumValue
    switch (status){
        case '1': statusEnumValue = 'Не относится к программе'
            break
        case '2': statusEnumValue = 'На рассмотрении'
            break
        case '3': statusEnumValue = 'В процессе ремонта'
            break
        case '4': statusEnumValue = 'Завершено'
            break
        case '5': statusEnumValue = 'Не рассмотрено'
            break
    }
    requests.getRequestsByStatus(statusEnumValue,(error,result)=>{
        if (error){
            console.error("status update error: ", error)
            res.render('error',{})
        }
        else {
            console.log("status updated: ", result)
            if (result.length>0){
                for (let i=0;i<result.length;i++){
                    let currentUserId = result[i].id_user
                    users.findUserById(currentUserId,(error,usersResult)=>{
                        if (error || usersResult.length<1) {
                            console.error("user shit happened: ",error)
                            res.render('error',{})

                        }
                        else {
                            result[i].username =usersResult[0].name
                        }
                    })
                }
            }
            ejs.renderFile(__dirname + '/../views/gos_org_profile/inbox_table.ejs',{applications : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})

router.get('/comment/:id',(req,res,next)=>{
    let requestId = req.param('id')
    comments.getCommentByRequest(requestId,(error,result)=>{
        if (error) {
            console.error("comments fetch error: ", error)
            res.render('error',{})
        }
        else {
            console.log("comments fetched: ", result)
            if (result.length>0){
                for (let i=0;i<result.length;i++){
                    let currentUserId = result[i].id_user
                    users.findUserById(currentUserId,(error,usersResult)=>{
                        if (error || usersResult.length<1) {
                            console.error("user shit happened: ",error)
                            res.render('error',{})

                        }
                        else {
                            result[i].username =usersResult[0].name
                        }
                    })
                }
            }
            ejs.renderFile(__dirname + '/../views/user_profile/comments_block.ejs',{comments : result},'cache',function (error, str) {
                if (!error){
                    res.send(str);

                }else {
                    console.log("shit happened : "+ error)
                    res.render('error',{})
                }

            });
        }
    })
})

router.post('/comment',(req,res,next)=>{
    let id = req.body.id
    let userId= req.body.userid
    let comment = req.body.comment
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    comments.addComment(userId,id,comment,date,(error,result)=>{
        if (error) {
            console.error("comment upload error: ", error)
            res.send({})
        }
        else {
            console.log("comment added: ", result)
            res.send(result)
        }
    })
})

router.post('/upload',(req,res,next)=>{
    let id = req.body.id
    let type = req.body.type
})

router.post('/sendrequest',(req,res,next)=>{
    let description = res.body.description
    let userid = re.body.userid
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    requests.addRequest(userid,0," "," ",description,date,(error,resp)=>{
        if(!error){
            res.send(resp)
        }else {
            console.error("shit happened: ", error)
            res.send({})
        }
    })
})
router.post('/setpriority',(req,res,next)=>{
    let id = req.body.id
    let priority = req.body.priority
    let priorityEnum;
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    switch (priority){
        case 1:priorityEnum = 'Низкий'
            break
        case 2:priorityEnum = 'Средний'
            break
        case 3:priorityEnum = 'Высокий'
            break
    }
    requests.updateRequestPriority(id,priorityEnum,date,(error,result)=>{
        if (error) console.error("priority update error: ", error)
        else {
            console.log("priority updated: ", result)
            res.send(result)
        }
    })
})

router.post('/setresponsible',(req,res,next)=>{
    let id = req.body.id
    let userId = req.body.userid
    let date = new Date().toISOString().slice(0, 9).replace('T', ' ')
    requests.updateRequestResponsible(id,userId,date,(error,result)=>{
        if (error) console.error("responsible update error: ", error)
        else {
            console.log("responsible updated: ", result)
            res.send(result)
        }
    })
})

module.exports = router