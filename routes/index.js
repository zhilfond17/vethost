let subjects = require('../models/subject')
let regions = require('../models/region')
let cities = require('../models/city')
let houses = require('../models/house')
let searchEngine = require('../models/searchEngine')
let events = require('events')
let eventEmitter = new events.EventEmitter()
const express = require('express');
const router = express.Router();
const ejs = require('ejs');
const fs = require('fs');

let buildingsGeoJson;
let buildings = require('../models/building')
const citiesIndex = 'cities'
const regionIndex = 'regions'
const housesIndex = "houses"
const subjectsIndex = 'subjects'

// fs.readFile('./public/resчources/building_vol.geojson', function (err, data) {
//     if (err) console.error('can\'t read geojson',err)
//     else {
//         buildingsGeoJson = JSON.parse(data).features;
//         let sum=0;
//         houses.getAllHouses((result)=>{
//             for (let i=0;i<result.length;i++ ){
//                 let actual = result[i]
//                 let long = actual.lon
//                 let lat = actual.lat
//
//                 for (let j=0;j<buildingsGeoJson.length;j++){
//                     let geoJson = buildingsGeoJson[j]
//                     if (d3.geoContains(geoJson,[long,lat])) {
//                         sum ++
//                         let osmId = geoJson.properties.OSM_ID
//                         let id = actual.id
//                         buildings.addBuilding(id,osmId,(error,result)=>{
//                             if (!error){
//                                 console.log("added: ",result)
//                             }
//                         })
//                         console.log("found: ",geoJson.properties.A_STRT+ "of : "+actual.street  )
//                     }
//                 }
//
//
//
//             }
//             console.log("finish")
//         })
//
//         // for(let i=0;i<buildingsGeoJson.length;i++){
//         //     let prop = buildingsGeoJson[i].properties
//         //     // if (prop.A_STRT==null && prop.A_HSNMBR == null) {
//         //     //     buildingsGeoJson.splice(i,1)
//         //     // }
//         // }
//
//
//     }
//
// })

// let parsedSites = JSON.parse("[{ \"id\" : 1, \"web\" : \"http://www.babaevo-adm.ru\" }, { \"id\" : 2, \"web\" : \"http://www.babushadm.ru\" }, { \"id\" : 3, \"web\" : \"https://belozer.ru\" }, { \"id\" : 4, \"web\" : \"http://lipinbor.ru\" }, { \"id\" : 5, \"web\" : \"http://www.vumr.ru\" }, { \"id\" : 6, \"web\" : \"http://www.adm-verhov.ru\" }, { \"id\" : 7, \"web\" : \"http://www.vozhega.ru\" }, { \"id\" : 8, \"web\" : \"http://www.volraion.ru\" }, { \"id\" : 9, \"web\" : \"http://vytegra.munrus.ru\" }, { \"id\" : 10, \"web\" : \"http://www.gradm.ru\" }, { \"id\" : 11, \"web\" : \"http://adminkaduy.ru\" }, { \"id\" : 12, \"web\" : \"http://kirillov-adm.ru\" }, { \"id\" : 13, \"web\" : \"http://kichgorod.ru\" }, { \"id\" : 14, \"web\" : \"http://www.mr35.ru\" }, { \"id\" : 15, \"web\" : \"http://www.nikolskreg.ru\" }, { \"id\" : 16, \"web\" : \"http://nyuksenitsa.ru\" }, { \"id\" : 17, \"web\" : \"http://www.sokoladm.ru\" }, { \"id\" : 18, \"web\" : \"http://сямженский-район.рф\" }, { \"id\" : 19, \"web\" : \"http://tarnoga-region.ru\" }, { \"id\" : 20, \"web\" : \"http://totma-region.ru\" }, { \"id\" : 21, \"web\" : \"http://www.kubena35.ru\" }, { \"id\" : 22, \"web\" : \"http://ustyzna.ru\" }, { \"id\" : 23, \"web\" : \"http://www.haradm.ru\" }, { \"id\" : 24, \"web\" : \"http://www.chagoda.ru\" }, { \"id\" : 25, \"web\" : \"https://cherra.ru\" }, { \"id\" : 26, \"web\" : \"http://www.sheksnainfo.ru\" }]");
//
// for (let i = 0; i < parsedSites.length; i++) {
//     let id = parsedSites[i].id
//     let web = parsedSites[i].web
//     console.log("parsed: ", id + ": " + web)
//     regions.addSite(id, web, (error, result) => {
//         if (!error) console.log("added id: ", result)
//         else console.error("shit happened with ", id + ": " + error);
//     })
// }

/* GET home page. */

/*----------------Главная страница-----------------------*/
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Добро пожаловать в Мой Дом'});
});

router.get('/buttons_damage_percent', function (req, res, next) {
    res.render('map_view/buttons_damage_percent', {title: ''});
});

router.get('/buttons_build_date', function (req, res, next) {
    res.render('map_view/buttons_build_date', {title: ''});
});

router.get('/top-left-panels', function (req, res, next) {
    res.render('top-left-panels', {title: ''});
});

/*TODO по возможности удалить*/
router.get('/home-left-panel', function (req, res, next) {
    res.render('home-left-panel', {title: ''});
});

router.get('/home-info', function (req, res, next) {
    res.render('main_views/home-info', {title: ''});
});

/*TODO скетч. По возможности удалить*/
router.get('/filter', function (req, res, next) {
    res.render('filters_page', {title: ''});
});


/*----------------Профиль пользователя-----------------------*/
router.get('/applications_tab', function (req, res, next) {
    res.render('user_profile/applications_tab', {title: ''});
});

router.get('/left-panel-1', function (req, res, next) {
    res.render('user_profile/left-panel-1', {title: ''});
});

router.get('/profile', function (req, res, next) {
    res.render('user_profile/profile', {title: ''});
});

router.get('/main_profile', function (req, res, next) {
    res.render('user_profile/main_profile', {title: ''});
});

router.get('/user_app', function (req, res, next) {
    res.render('user_profile/user_app', {title: ''});
});
/*----------------Профиль муниципального служащего-----------------------*/
router.get('/organization_profile', function (req, res, next) {
    res.render('gos_org_profile/organization_profile', {title: ''});
});

router.get('/inbox_table', function (req, res, next) {
    res.render('gos_org_profile/inbox_table', {title: ''});
});

router.get('/mail_mun', function (req, res, next) {
    res.render('gos_org_profile/mail_mun', {title: ''});
});

router.get('/process_application', function (req, res, next) {
    res.render('gos_org_profile/process_application', {title: ''});
});

router.get('/map-content', function (req, res, next) {
    res.render('gos_org_profile/map-content', {title: ''});
});


/*----------------Поисковый запрос-----------------------*/
router.get('/search', function (req, res, next) {
    var id = req.body.id;

//    TODO: search elastic return full name type of address and ID in the db

    var type = req.body.type;
    var fileToRender;
    var yearPanel;
    if (type === 1) fileToRender = 'left-panel-1.ejs'
    else if (type === 2) {
        fileToRender = 'left-panel-2.ejs'
        yearPanel = 'years-panel.ejs'
    }
    var response = ejs.renderFile('views/' + fileToRender, {}, 'cache', function (err, html) {
        var data = {}
        //TODO: add error to response
        res.send({data: data, html: html})
    })
})

router.get('/modal/house/:id', function (req, res) {
    var id = req.params.id;
    console.log("ID of a house " + id)

    houses.findHouse(id, function (result) {
        let house = result[0]

        let json = {address:"ул. " + house.street + " " + house.h_number, build_year:house.build_year, repair_date_start:house.last_fix_start, repair_date_end : house.last_fix_end,
            planning_repairment_start: house.future_fix_start, planning_repairment_end: house.future_fix_end,
            last_electro: house.last_electro, future_electro_start:house.future_electro_start, future_electro_end:house.future_electro_end,
            last_warm: house.last_warm, future_warm_start:house.future_warm_start, future_warm_end:house.future_warm_end,
            last_gaz: house.last_gaz, future_gaz_start:house.future_gaz_start, future_gaz_end:house.future_gaz_end,
            last_water:house.last_water, future_water_start:house.future_water_start, future_water_end:house.future_water_end}
        ejs.renderFile(__dirname + '/..' + '/views/main_views/home-info.ejs', json, "cache", function (err, html) {
            if (err) {
                console.error("Shit happened", err)
            } else {
                res.send({html:html, json:json})
            }
    })
})
})

router.get('/search/:type/:id', function (req, res) {
    let id = req.params.id;
    let type = req.params.type;
    console.log(' param: ', req.params)

    switch (type) {
        case 'дом':
            houses.findHouse(id, function (result) {
                let house = result[0]
                console.log(house)
                //planning_repairment_start, planning_repairment_end,
                // repair_date_start (Дата постановки на ремонт), address
                let json = {
                    address: "ул. " + house.street + " " + house.h_number,
                    build_date: house.build_year,
                    repair_date_start: house.last_fix_start,
                    repair_date_end: house.last_fix_end,
                    planning_repairment_start: house.future_fix_start,
                    planning_repairment_end: house.future_fix_end,
                    osm_id: house.osm_id
                }
                ejs.renderFile(__dirname + '/..' + '/views/main_views/home-left-panel.ejs', json, "cache", function (err, html) {
                    if (err) {
                        console.error("Shit happened", err)
                        ejs.renderFile(__dirname+ '/..' + '/views/error.ejs',{message : 'Упс!! ',error:{status: ' ((', stack: 0}},'cache',(errorerror, htmlError)=>{
                                if(errorerror){
                                    console.error("shite happned: ", errorerror)
                                    res.send({})
                                }else {
                                    res.send(htmlError)
                                }
                        }

                        )
                    } else {
                        res.send({html: html, json: json})
                    }
                })
            })
            break;
        case 'город':
            cities.findCityById(id, function (result) {
                let city = result[0]
                getDensity(city.population, city.area, (density) => {
                    let json = {title: city.title, people: city.population, area: city.area, density: density}
                    ejs.renderFile(__dirname + '/..' + '/views/main_views/top-left-panels.ejs', json, "cache", function (err, html) {
                        if (err) {
                            console.error("Shit happened", err)
                            res.render('error',{})
                        } else {
                            res.send({html: html, json: json})
                        }
                    })
                })

            })
            break;
        case 'регион':
            regions.findRegionById(id, function (result) {
                let region = result[0]
                getDensity(region.population, region.area, (density) => {
                    let json = {
                        title: region.title,
                        people: region.population,
                        area: region.area,
                        density: density,
                        website: region.website
                    }
                    ejs.renderFile(__dirname + '/..' + '/views/main_views/top-left-panels.ejs', json, "cache", function (err, html) {
                        if (err) {
                            console.error("Shit happened", err)
                            res.render('error',{})
                        } else {
                            res.send({html: html, json: json})
                        }
                    })
                })
            })
            break;
        case 'субъект':
            subjects.findSubjectById(id, function (result) {
                let subject = result[0]
                getDensity(subject.population, subject.area, (density) => {
                    let json = {title: subject.title, people: subject.population, area: subject.area, density: density}
                    ejs.renderFile(__dirname + '/..' + '/views/main_views/top-left-panels.ejs', json, "cache", function (err, html) {
                        if (err) {
                            console.error("Shit happened", err)
                            res.render('error',{})
                        } else {
                            res.send({html: html, json: json})
                        }
                    })
                })
            })
            break;

    }

});
router.get('/getHousesByYear', function (req, res, next) {
    var buildYear = req.body.build;
    var repair = req.body.repair;

})

//This route is intended to search and return the list of closest term not for autocomplete
router.get('/searchElastic',(req,res,next)=>{
    let phrase = req.param('term')
    console.log("phrase :", phrase)
    searchEngine.phraseSuggestion(phrase,(result)=>{
        console.log("result : ", result)
        let phrases = [];
        try {
            result = JSON.parse(result)
            let hits = result.hits.hits
            for (let i=0;i<hits.length;i++){
                let current = hits[i]
                let id = current._id
                let label = current._source.title
                let type = current._index
                let desc
                switch (type){
                    case subjectsIndex:
                        desc = 'субъект'
                        label = label + ' область'
                        console.log('type',desc)
                        break;
                    case regionIndex:
                        desc = 'регион'
                        label = label + ' район'
                        console.log('type',desc)
                        break;
                    case citiesIndex:
                        desc = 'город'
                        label = 'г. ' + label
                        console.log('type',desc)
                        break;
                    case housesIndex:
                        desc = 'дом'
                        label = 'д. '+ label
                        console.log('type',desc)
                        break;
                }

                phrases.push({value: id, label : label, desc: desc})

            }

            res.header("Access-Control-Allow-Origin", "*")
            res.send(phrases)
        }catch (syntaxError){
            console.error("result parsing error: ",syntaxError)
            res.header("Access-Control-Allow-Origin", "*")
            res.send(phrases)
        }
    })
})


router.get('/addresses/:input',(req,res,next)=>{
    let input = req.params.input;
    let id;
    let desc;
    let type;


        searchEngine.suggest(input,(result)=>{
            try {
                result = JSON.parse(result);

                if (input.indexOf(' ') >-1){
                    let hits = result.hits.hits
                    if (hits.length>0){
                        let current = hits[0]
                        id = current._id
                        desc = current._index
                    }else {
                        ejs.renderFile(__dirname+ '/..' + '/views/error.ejs',{message : 'Упс!! ',error:{status: ' ((', stack: 0}},'cache',(errorerror, htmlError)=>{
                                if(errorerror){
                                    console.error("shite happned: ", errorerror)
                                    res.send({})
                                }else {
                                    res.send(htmlError)
                                }
                            }

                        )
                    }

                }
                else {
                    let sugs= result.suggest.address_suggestions
                    if (sugs.length>0){
                        let current = sugs[0].options[0]
                        if (typeof current != undefined){
                            console.log("the current: ", current)
                            id = current._id
                            desc = current._index
                        }else {
                            ejs.renderFile(__dirname+ '/..' + '/views/error.ejs',{message : 'Упс!! ',error:{status: ' ((', stack: 0}},'cache',(errorerror, htmlError)=>{
                                    if(errorerror){
                                        console.error("shite happned: ", errorerror)
                                        res.send({})
                                    }else {
                                        res.send(htmlError)
                                    }
                                }

                            )
                        }


                    }
                }
                switch (desc){
                    case subjectsIndex:
                        type = 'субъект'
                        break;
                    case regionIndex:
                        type = 'регион'
                        break;
                    case citiesIndex:
                        type = 'город'
                        break;
                    case housesIndex:
                        type = 'дом'
                        break;
                }
                console.log("found this address : ", id+ " "+ type)
                if(typeof id == 'undefined'|| typeof type == 'undefined'){
                    ejs.renderFile(__dirname+ '/..' + '/views/error.ejs',{message : 'Упс!! ',error:{status: ' ((', stack: 0}},'cache',(errorerror, htmlError)=>{
                            if(errorerror){
                                console.error("shite happned: ", errorerror)
                                res.send({})
                            }else {
                                res.send(htmlError)
                            }
                        }

                    )
                }else {
                    req.url = '/search/'+type+'/'+id
                    router.handle(req,res,next)
                }

            }catch (syntaxError){
                console.error("result parsing error: ",syntaxError)
                res.header("Access-Control-Allow-Origin", "*")
                res.render('error',{})
            }

        })

    })
router.get('/addresses', function (req, res) {

    let termOrPhrase = req.param('term');
    // let terms = term.split(" ")

    // let term = req.params.term
    console.log("term: ", termOrPhrase)

    if (termOrPhrase.trim().indexOf(' ') >-1){
        req.url ='/searchElastic'
        router.handle(req,res)
    }else {
        searchEngine.termSuggest(termOrPhrase,(result)=>{
            let suggestions = [];
            console.log("result", result)
            try{
                result =JSON.parse(result)
                let sugs= result.suggest.address_suggestions
                if (sugs.length>0){
                    for (let i=0; i<sugs.length;i++){

                        let currentSuggestion  = sugs[i].options
                        for (let i=0;i<currentSuggestion.length;i++){
                            let current = currentSuggestion[i]
                            let id = current._id
                            let label = current._source.title
                            let type = current._index
                            let desc
                            switch (type){
                                case subjectsIndex:
                                    desc = 'субъект'
                                    label = label + ' область'
                                    console.log('type',desc)
                                    break;
                                case regionIndex:
                                    desc = 'регион'
                                    // label = label + ' район'
                                    console.log('type',desc)
                                    break;
                                case citiesIndex:
                                    desc = 'город'
                                    // label = 'г. ' + label
                                    console.log('type',desc)
                                    break;
                                case housesIndex:
                                    desc = 'дом'
                                    // label = 'д. '+ label
                                    console.log('type',desc)
                                    break;
                            }
                            suggestions.push({value: id, label : label, desc: desc})
                        }

                    }
                    res.header("Access-Control-Allow-Origin", "*")
                    res.send(suggestions)
                }
            }catch (syntaxError){
                console.error("result parsing error: ",syntaxError)
                res.header("Access-Control-Allow-Origin", "*")
                res.send(suggestions)
            }

        })
    }



})

//TODO: сделайте нормальный обработчик запросов для OSM_ID, чтобы он возвращал какой-нибудь результат в случае ошибки, либо как-то оповещал
router.get('/modal/:osmId', (req, res) => {
    let osmId = req.param('osmId');
    houses.findHouseByOSMId(osmId, (error, result) => {
        if (!error && result.length > 0) {
            let house = result[0];
            let json = {
                address: "ул. " + house.street + " " + house.h_number,
                build_year: house.build_year,
                repair_date_start: house.last_fix_start,
                repair_date_end: house.last_fix_end,
                planning_repairment_start: house.future_fix_start,
                planning_repairment_end: house.future_fix_end,
                last_electro: house.last_electro,
                future_electro_start: house.future_electro_start,
                future_electro_end: house.future_electro_end,
                last_warm: house.last_warm,
                future_warm_start: house.future_warm_start,
                future_warm_end: house.future_warm_end,
                last_gaz: house.last_gaz,
                future_gaz_start: house.future_gaz_start,
                future_gaz_end: house.future_gaz_end,
                last_water: house.last_water,
                future_water_start: house.future_water_start,
                future_water_end: house.future_water_end,
                osm_id: house.osm_id
            };
            //        TODO: Лида : скажи что делать с этими даннами
            ejs.renderFile(__dirname + '/..' + '/views/main_views/home-info.ejs', json, "cache", function (err, html) {
                if (err) {
                    console.error("Shit happened", err)
                } else {
                    res.send({html: html, json: json})
                }
            });
        } else {
            console.log("There is no house: " + req);
            let json = {
                address: "ул. X",
                build_year: "Нет информации",
                repair_date_start: null,
                repair_date_end: null,
                planning_repairment_start: null,
                planning_repairment_end: null,
                last_electro: null,
                future_electro_start: null,
                future_electro_end: null,
                last_warm: null,
                future_warm_start: null,
                future_warm_end: null,
                last_gaz: null,
                future_gaz_start: null,
                future_gaz_end: null,
                last_water: null,
                future_water_start: null,
                future_water_end: null,
                osm_id: null
            };
            ejs.renderFile(__dirname + '/..' + '/views/main_views/home-info.ejs', json, "cache", function (err, html) {
                if (err) {
                    console.error("Shit happened", err)
                } else {
                    res.send({html: html, json: json})
                }
            });
        }
    })
});


function getDensity(people, area, cb) {
    let density = people / area
    cb(density.toFixed(2));
}


/**=============================================ELASTIC SEARCH=======================================**/
let queues = []

searchEngine.ping()

// searchEngine.initAllIndices((resp)=>{
//     console.log("response from indexing: ", resp)
//     searchEngine.initMapping((response1)=>{
//         console.log("Indices initialized: ", response1)
//         loadElementsForElasticIndex()
//
//     })
// })




function loadElementsForElasticIndex() {

    let counter= 0;
    subjects.findAllSubject((subjs) => {
        for (let i = 0; i < subjs.length; i++) {
            let actual = subjs[i]
            let desc = actual.title;
            let id = actual.id
            // elasticIndex(subjectsIndex,desc,id)
            queues.push({type : subjectsIndex, title : desc, id: id})
            if (i===subjs.length-1) counter++
            if (counter ===4) eventEmitter.emit('startIndexing');
        }

    })

    regions.getAllRegions(rgns=>{
        for (let i=0; i<rgns.length; i++){
            let actual = rgns[i]
            let desc = actual.title
            let id = actual.id
            // elasticIndex(regionIndex,desc,id)
            queues.push({type : regionIndex, title : desc, id: id})
            if (i===rgns.length-1) counter++
            if (counter ===4) eventEmitter.emit('startIndexing');
        }

    })

    cities.getAllCities(cts=>{
        for (let i=0; i<cts.length; i++){
            let actual = cts[i]
            let desc = 'город ' + actual.title
            let id = actual.id
            // elasticIndex(citiesIndex,desc,id)
            queues.push({type : citiesIndex, title : desc, id: id})
            if (i===cts.length-1) counter++
            if (counter ===4) eventEmitter.emit('startIndexing');
        }
    })

    houses.getAllHouses(hs=>{
        for (let i=0; i<hs.length; i++){
            let actual = hs[i]
            let desc = 'дом '+ actual.h_number +' '+ actual.street+' '+actual.mun_region
            let id = actual.id
            // elasticIndex(housesIndex,desc,id)
            queues.push({type : housesIndex, title : desc, id: id})
            if (i===hs.length-1) counter++
            if (counter ===4) eventEmitter.emit('startIndexing');
        }
    })

}


let pointer = 0;
let startIndexing = function() {
    console.log("indexing....")

    let currentDocument = queues[pointer]
    searchEngine.addIndex(currentDocument,currentDocument.type,(error,response)=>{
        if (!error) {
            console.log("document "+ currentDocument.id + " has been indexed",response)
            pointer++
            if (pointer<queues.length) eventEmitter.emit('startIndexing')
        }
    })
}
eventEmitter.on('startIndexing',startIndexing)

module.exports = router;
