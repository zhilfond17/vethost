let connection = require('../config/db')

class Region{
    static getAllRegions(cb){
        connection.query('SELECT * FROM mun_region', (err,result)=>{
            if (err) console.error('shit happened: ', err)
            else cb(result)
        })
    }


    static findRegionById(id,cb){
        connection.query('SELECT * FROM mun_region WHERE id = ? ',[id],(err, result)=>{
            if (err) console.error('shit happened: ',err)
            else cb(result)
        })
    }


    static autoComplete(term,cb){
        let terms = term.split(" ")
        let suggestions = [];
        for (let i=0;i<terms.length;i++){
            connection.query('SELECT * FROM mun_region WHERE title LIKE ?',['%'+term+'%'],(error,result)=>{
                if (error){
                    console.error("shit happened: ",error)
                    cb(error,suggestions)
                }else {
                    for(let j=0;j<result.length;j++) {
                        suggestions.push(result[j])
                        if (i===terms.length-1) {
                            console.log("suggesting regions")
                            cb(error,suggestions)
                        }
                    }
                }
            })
        }

    }

    static addSite(id,site,cb){
        connection.query('UPDATE mun_region SET website = ? WHERE id = ?',[site,id],(error,result)=>{
            cb(error,result);
        })
    }
}



module.exports = Region;