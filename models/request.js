let connection = require('../config/db')

class Request{
    static getAllRequests(cb){
        connection.query('SELECT * FROM requests',(cb))
    }

    static getRequest(id,cb){
        connection.query('SELECT * FROM requests WHERE id = ? ',[id],cb)
    }

    static updateRequestStatus(id,status,date,cb){
        connection.query('UPDATE requests SET status = ?, last_upd = ? WHERE id = ?',[status,date,id],cb)
    }

    static updateRequestPriority(id,priority,date,cb){
        connection.query('UPDATE requests SET priority = ?, last_upd = ? WHERE id = ?',[priority,date,id],cb)
    }

    static updateRequestResponsible(id,responsible,date,cb){
        connection.query('UPDATE requests SET responsible = ?, last_upd = ? WHERE id = ?',[responsible,date,id],cb)
    }

    static addRequest(userId,houseId,group,title,description,date,cb){
        connection.query('INSERT INTO requests SET title = ?,description = ?,group = ?, id_user = ?, id_house = ?, last_upd = ?',
            [title,description,group,userId,houseId, date],cb)
    }

    static getRequestByUser(userId, cb){
        connection.query('SELECT * FROM requests WHERE id_user = ?',[userId],cb)
    }

    static getRequestsByHouse(houseId, cb){
        connection.query('SELECT * FROM requests WHERE id_house = ?',[houseId],cb)
    }

    static getRequestsByStatus(status, cb){
        connection.query('SELECT * FROM requests WHERE status = ?',[status],cb)
    }
}

module.exports = Request