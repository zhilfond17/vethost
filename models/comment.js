let connection = require('../config/db')

class Comment{
    static getAllComments(cb){
        connection.query('SELECT * FROM comments',cb)
    }
    static getCommentByRequest(id,cb){
        connection.query('SELECT * FROM comments WHERE id_request = ? ORDER BY date',[id],cb)
    }

    static getCommentByUser(id,cb){
        connection.query('SELECT * FROM comments WHERE id_user = ? ORDER BY date',[id],cb)
    }

    static getCommentByUserOnRequest(userId,requestId,cb){
        connection.query('SELECT * FROM comments WHERE id_user = ? AND id_request = ? ORDER BY date',[userId, requestId],cb)
    }

    static addComment(userId,requestId,comment,date,cb){
        connection.query('INSERT INTO comments SET id_user = ?, id_request = ?, description = ?,date= ? ORDER BY date',[userId,requestId,comment,date],cb)
    }
}

module.exports = Comment