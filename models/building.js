let connection = require('../config/db')

class Building {

    static addBuilding(houseId,osmId, cb){
        connection.query('INSERT INTO buildings SET house_id = ?,osm_id = ?',[houseId, osmId],
            function (error, result, fields) {
            if (error){
                console.error('error adding building: ', error)
            }
            else cb(error,result)
        })
    }


    static findBuildingByOSMId(osmId, cb){
        connection.query('SELECT * FROM buildings WHERE osm_id = ?',[osmId],function (err, result, fields) {
            if (err){
                console.error('error querying building: ', err)
            }
            else cb(result)
        })
    }

    static findBuildingById(id, cb){
        connection.query('SELECT * FROM buildings WHERE house_id = ?',[id],function (err, result, fields) {
            if (err){
                console.error('error querying building: ', err)
            }
            cb(err,result)
        })
    }


    static  findAllBuilding(cb){
        connection.query('SELECT * FROM buildings',(err,result)=>{
            if (err) console.error('shit happened: ', err)
            else cb(result)
        })
    }




}


module.exports = Building
