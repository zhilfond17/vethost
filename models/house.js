let connection = require('../config/db')

class House{

    static findHouse(houseID, cb){
        connection.query('SELECT * FROM houses WHERE id = ?',[houseID], function (error, result, fields) {
            if (error){
                console.error('nothing found')

                throw error
            }
            // console.log('fields: ', fields)
            cb(result)


        })
    }

    static findHouseByStreet(street,house,cb){
        connection.query('SELECT * FROM houses WHERE street = ? AND h_number = ?',[street, house], function (error, result, fields) {
            if (error){
                console.error('nothing found')

            }
            // console.log('fields: ', fields)
            cb(result)


        })
    }

    static findByBuildedDateBetween(dateStart, dateEnd, cb){
        connection.query('SELECT * FROM houses WHERE build_year BETWEEN ?? AND ??', function (error, result, fields) {
            if (error){
                console.error('error looking for houses: ', error)
            }
            console.log('fiels: ', fields)
            cb(result)
        })
    }

    static findByRepairedDate(dateStart, dateEnd, cb){
        connection.query('SELECT * FROM houses WHERE future_fix_start BETWEEN ?? AND ?? UNION SELECT * FROM houses WHERE future_fix_end BETWEEN ?? AND ??',[dateStart, dateEnd,dateStart,dateEnd], function (error, result, fields) {
            if (error){
                console.error('error looking for houses: ', error)
            }
            // console.log('fiels: ', fields)
            cb(result)
        })
    }

    static findByDamagePercentage(startInterval, endInterval, cb){
        connection.query('SELECT * FROM houses WHERE wearout BETWEEN ?? AND ??',[startInterval, endInterval], function (error, result, fields) {
            if (error){
                console.error('error looking for houses: ', error)
            }
            cb(result)
        })
    }

    static getAllHouses(cb){
        connection.query('SELECT * FROM houses',function (err, result, fields) {
            if (err) console.error("shit happened: ", err)
            else {
                cb(result)
            }
        })
    }


    static findHouseByOSMId(osmId, cb){
        connection.query('SELECT * FROM houses WHERE osm_id = ?',[osmId],(error, result)=>{
            if (error) console.error("shit happened: ", error)
            cb(error,result);
        })
    }


    static autoComplete(term,cb){
        let terms = term.split(" ")
        let suggestions = [];

        for (let i=0;i<terms.length;i++){
            connection.query('SELECT * FROM houses WHERE mun_region LIKE ? OR city LIKE ? OR street LIKE ?',['%'+term+'%','%'+term+'%','%'+term+'%'],(error,result)=>{
                if (error){
                    console.error("shit happened: ",error)
                    cb(error,suggestions)
                }else {
                    for(let j=0;j<result.length;j++) {
                        suggestions.push(result[j])
                        if (i===terms.length-1) {
                            console.log("sending houses")
                            cb(error,suggestions)
                        }
                    }

                }
            })
        }
        // connection.query('SELECT * FROM houses WHERE mun_region LIKE ? OR city LIKE ? OR street LIKE ?',['%'+term+'%','%'+term+'%','%'+term+'%'],(error,result)=>{
        //     if (error){
        //         console.error("shit happened: ",error)
        //         cb(error)
        //     }else {
        //         cb(error,result);
        //     }
        // })
    }
}

module.exports = House;